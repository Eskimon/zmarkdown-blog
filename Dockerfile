FROM node:8-alpine
MAINTAINER Eskimon <eskimon@outlook.com>

RUN apk add --update git

WORKDIR /app

RUN git config --global user.email "dummy-docker@example.com"
RUN git config --global user.name "Dummy Docker"

# Setup zmarkdown
RUN git clone https://gitlab.com/Eskimon/zmarkdown-blog.git \
    && cd zmarkdown-blog \
    && git checkout master \
    && git pull \
    && git branch dev \
    && git checkout dev \
    && npm install --production \
    # Add blog config
    && git cherry-pick 0472dea9025b61bde45f9a54eb34854b808f707b \
    # Tweak zmarkdown behavior
    && git cherry-pick 4c02a71dc0d0720d792afc9e53a00f32bfdee4d2

# Make port 27272 available to the world outside this container
EXPOSE 27272

RUN npm install pm2 -g
WORKDIR /app/zmarkdown-blog/node_modules/zmarkdown
CMD ["pm2-runtime", "start", "server/index.js"]