install:
	git checkout master
	git pull
	git branch dev
	git checkout dev
	npm install --production
	# Add blog config
	git cherry-pick 0472dea9025b61bde45f9a54eb34854b808f707b
	# Tweak zmarkdown behavior
	git cherry-pick 4c02a71dc0d0720d792afc9e53a00f32bfdee4d2

start:
	cd node_modules/zmarkdown && npm run server

stop:
	cd node_modules/zmarkdown && npm run stop
